#!/usr/bin/env python
# -*- coding: utf-8 -*-
# 
# Copyright 2014 speirs <vvebmaster _at_ codereading.com>
# Copyright 2010-2012 Shin-ya Murakami <murashin _at_ gfd-dennou.org>
#
# ツリー構造を表現したcsvファイルから yEd GraphML ファイルを生成するスクリプト
#
# Converts CSV file with a tree-like format to GraphML
# (an XML DTD that is used by the yEd_ graph editor).
#

import csv
import os
import sys
import cgi


# 設定
id_prefix = 'id' # nodeのシンボルのprefix
id_digits = 5    # nodeの数の桁数

# XMLコード用に文字をエスケープ
def escape_dq(s):
    return cgi.escape(s)
#end def escape_dq


# nodeのシンボルを定義
def id(nid, prefix=None):
    global id_prefix, id_digits

    if prefix is None:
        ipref = id_prefix
    else:
        ipref = prefix

    return "%s%s" % (ipref, str(nid).rjust(id_digits, '0'))
#end def id


def define_page_node(nid, label):
    node = '''
    <node id="%s">
      <data key="d0">
        <y:ShapeNode>
          <y:Geometry height="30.0" width="30.0" x="-15.0" y="-15.0"/>
          <y:Fill hasColor="false" transparent="false"/>
          <y:BorderStyle color="#000000" type="line" width="1.0"/>
              <y:NodeLabel alignment="center" autoSizePolicy="content" fontFamily="Dialog" fontSize="10" fontStyle="plain" hasBackgroundColor="false" hasLineColor="false" height="18.1328125" modelName="custom" textColor="#000000" visible="true" width="11.587890625" x="9.2060546875" y="5.93359375">%s<y:LabelModel>
              <y:SmartNodeLabelModel distance="4.0"/>
            </y:LabelModel>
            <y:ModelParameter>
              <y:SmartNodeLabelModelParameter labelRatioX="0.0" labelRatioY="0.0" nodeRatioX="0.0" nodeRatioY="0.0" offsetX="0.0" offsetY="0.0" upX="0.0" upY="-1.0"/>
            </y:ModelParameter>
          </y:NodeLabel>
          <y:Shape type="rectangle"/>
        </y:ShapeNode>
      </data>
    </node>
''' % (nid, label)
    return node
#end define_page_node


def define_link_edge(eid, source_id, target_id):
    edge = '''
    <edge id="%s" source="%s" target="%s">
      <data key="d1">
        <y:PolyLineEdge>
          <y:Path sx="0.0" sy="0.0" tx="0.0" ty="0.0"/>
          <y:LineStyle color="#999999" type="line" width="1.0"/>
          <y:Arrows source="none" target="none"/>
          <y:BendStyle smoothed="false"/>
        </y:PolyLineEdge>
      </data>
    </edge>
''' % (eid, source_id, target_id)
    return edge
#end def define_link_edge


# nodeを定義する出力
def define_node(nid, label):
    print define_page_node(id(nid), label)
    nid += 1
    return nid
#end def define_node


# node間の関係を定義する出力
def define_relation(id_a, id_b):
    eid = 'e%d-e%d' % (id_a, id_b)
    print define_link_edge(eid, id(id_a), id(id_b))
#end def define_relation


# ヘッダとフッタの定義
def print_header():
    print """<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<graphml xmlns="http://graphml.graphdrawing.org/xmlns" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:y="http://www.yworks.com/xml/graphml" xmlns:yed="http://www.yworks.com/xml/yed/3" xsi:schemaLocation="http://graphml.graphdrawing.org/xmlns http://www.yworks.com/xml/schema/graphml/1.1/ygraphml.xsd">
  <key id="d0" for="node" yfiles.type="nodegraphics"/>  
  <key id="d1" for="edge" yfiles.type="edgegraphics"/>  
  <graph edgedefault="directed" id="G">
"""
#end def print_header


def print_footer():
    print """
   </graph>
</graphml>
"""
#end def print_footer


# 引数チェック
try:
    inputfile = sys.argv[1]
except:
    print "error: need an argument of input file."
    sys.exit(1)

# メインの処理
nid = 1          # 何番のidまで使ったかを表す数
is_first_line = True

# pathは, CSVファイルの中身が表現しているtree構造において, 
# rootから, あるbranch/leafまでのnodeを順に並べた配列である. 
path = []

csv_reader = csv.reader(open(inputfile, 'rb'))

for row in csv_reader:
    # 空行の読み飛ばし
    if len(filter(None, row)) == 0:
        continue
    # コメント行の読み飛ばし
    if filter(None, row)[0]:
        if filter(None, row)[0].startswith('#'):
            continue

    # 最初に登場するゼロでない要素をタイトルとする
    if is_first_line:
       title = filter(None, row)[0]
       print_header()  # ヘッダを出力
       is_first_line = False
       continue
  
    cur = 0  # path配列において注目するインデックスの位置

    for col in row:
        if col and col.strip():
            col = col.strip()  # 前後の空白を除去

        if col:
            # path配列の, indexがcur以降の要素を削除
            if cur == 0:
                path = []
            else:
                path = path[0:cur]
            path.append(nid) # pathの末尾にnidを追加

            nid = define_node(nid, col) # ノードの定義
            if len(path) > 1:
                # path配列の要素が2個以上の場合は, 関係を定義
                define_relation(path[-2], path[-1])
        cur += 1 # 現在位置更新

print_footer()
