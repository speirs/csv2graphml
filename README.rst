###############################################################################
csv2graphml
###############################################################################

階層関係を表現した CSV ファイルを yEd_ GraphML ファイルに変換するスクリプトです。

Converts CSV file with a tree-like format to GraphML
(an XML DTD that is used by the yEd_ graph editor).


使い方 / Usage
===============================================================================
::

    python csv2graphml.py tree.csv > output.graphml

作成された GraphML ファイルは yEd_ グラフ エディタで編集できます。

You can edit GraphML file by yEd_ graph editor.


.. admonition:: 注意点 / Note
    :class: note

    すべてのノードは原点にあり、サイズも標準の 30x30 になっています。
    エッジはポリラインです。
    
    The nodes and edges of the graph are now all centered to the origin. 
    The nodes have a standard size of 30x30.
    The edges style is a polyline.


関連サイト / Resources
===============================================================================
* treecsv2dot_ - 階層関係を表現した CSV ファイルを DOT ファイルに変換する。Converts CSV file with a tree-like format to DOT file.
* dottoxml_ - DOT ファイルを yEd GrapML に変換する。Converts DOT graph files to Graphml. 
* yEd_ - 最強のグラフエディタ。Powerful graph editor.

.. _dottoxml: http://dl9obn.darc.de/programming/python/dottoxml/
.. _treecsv2dot: https://github.com/murashin/treecsv2dot
.. _yEd: http://www.yworks.com/en/products_yed_about.html
